import styled from 'styled-components';
import Buttom from './button';
import MiniCard from './miniCard';

const Container = styled.div<{ image: string }>`
  position: relative;  

  /* background-image: url(${({ image }) => image});
  background-size: cover; */
  
  /* background-color: #111114; */
  height: 100vh;
  overflow: hidden;


  .mask {
    position: absolute;
    inset: 0;
    background-color: #0a0a0a4b;
  }

  .body {
    height: 60vh;
    background-color: #0a0a0aed;
    background-color: #111114;

    padding: 16px;
    display: flex;
    flex-direction: column;
    gap: 20px;
  }

  .title{
    color: white;
    margin: 0;
    font-size: 38px;
  }
  .description{
    color: #cfced1;
    margin: 0;
  }
  .button {
  }
  .swipe {
    display: flex;
    gap: 16px;
  }

  .footer {
    margin: 0 12px;
    display: flex;
    gap: 32px;
  }

  
  .header {
    position: relative;
    width: 100%;
    height: 40vh;

    background-image: url(${({ image }) => image});
    background-size: cover;
    background-position: center;

    background-color: #111114;

    .cover {
      position: absolute;
      bottom: 10px;
      width: 100%;
      display: flex;
      justify-content: space-around;
      .star {
        color: white;
      }
    }
  }
`;

export interface params {
  image: string
  onClick?: () => void
};

const App = (params: params): JSX.Element => {
  return (
    <Container image={params.image}>
      <div className='header'>
        {/* <img src={params.image} className='cover'></img> */}
        <div className='mask' />

        <div className='cover'>
          <h2 className='title'>Costa Coffee</h2>
          <p className='star'>4.5</p>
        </div>

      </div>

      <div className='body'>
        <p className='description'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quidem unde dolores deserunt? Aliquid consequatur accusamus a quidem ducimus pariatur, repudiandae quam minus ab consequuntur vitae, reiciendis aspernatur eveniet exercitationem?</p>
        <div className='footer'>
          <p className='description'>4 personas</p>
          <p className='description'>Envio a domicilio</p>

        </div>
        <Buttom label={'Solicitar pedido'} />
        <div className='swipe'>
          <MiniCard image='https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80' title='Guiso' description='15% off' onclick={params.onClick}/>
          <MiniCard image='https://images.unsplash.com/photo-1565299624946-b28f40a0ae38?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=481&q=80' title='Pizza' description='15% off' />
        </div>
      </div>
    </Container>
  )
}

App.defaultProps = {
  onclick: () => null
};

export default App;
