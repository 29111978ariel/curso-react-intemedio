import { CSSProperties, useState } from 'react';
import styled from 'styled-components';
import Button from '../component/button';
// import MiniCard from './miniCard';

const Container = styled.div<{ image: string, visible: boolean }>`
  top: 0;
  position: absolute;
  width: 100%;
  height: 100vh;

  background-image: url(${({ image }) => image});
  background-size: 180%;
  background-position: top right ;
  background-repeat: no-repeat;

  background-color: #111114;

  transform: translateX(${({visible}) => visible ? '100vw' : '0'});
  transition: transform .4s ease-in-out;

.card {
  position: absolute;
  height: 40vh;
  background-color: #0a0a0ae6;
  width: 100vw;
  bottom: 0;
  border-radius: 7% 7% 0 0 ;
  padding: 16px;
  backdrop-filter: blur(4px);

  display: flex;
  flex-direction: column;
  gap: 16px;
}

.title{
    color: white;
    margin: 0;
    font-size: 38px;
  }
  .description{
    color: #cfced1;
    margin: 0;
  }

  .button {
    width: 65%;
  }
  .btn-back{
    background-color: white;
    border: 1px solid white;
    color: black;
    width: 30%;
  }
  .row {
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 16px;
  }
  .counter {
    color: white;
    font-size: 28px;
  }

  .btn-operador {
    background-color: #303030;
    border: 1px solid #303030;
    width: 45px;
  }
  .price {
    color: white;
    font-size: 30px;
  }
`;

export interface params {
  visible: boolean
  price: number
  title: string
  description: string
  image: string
  onClick?: () => void
};

const App = (params: params): JSX.Element => {
  const [ counter, setCounter ] = useState<number>(1);

  const handleAdd = () => {setCounter(counter+1)}

  const handleSus = () => counter > 0 && setCounter(counter-1)

  return (
    <Container image={params.image} visible={params.visible} >
      <div className='card'>
        <h2 className='title'>{params.title}</h2>
        <p className='description'>{params.description} </p>

        <div className='row'>
          <div className='row'>
            <Button className='btn-operador' label='-' onClick={handleSus} />
            <p className='counter'>{counter}</p>
            <Button className='btn-operador' label='+' onClick={handleAdd} />
          </div>

          <p className='price'>$ {counter*params.price}</p>

        </div>

        <div className='row'>
          <Button label='Pagar' />
          <Button className='btn-back' label='Volver' onClick={params.onClick} />

        </div>

      </div>
    </Container>
  )
}

App.defaultProps = {
  onClick: () => null
};

export default App;
