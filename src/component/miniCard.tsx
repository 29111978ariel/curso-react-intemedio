import styled from 'styled-components';

const Container = styled.div<{image: string}>`
  height: 200px;
  width: 300px;
  min-width: 200px;
  border-radius: 10px;

  position: relative;

  background-image: url(${({image}) => image});
  background-size: cover;
  background-position: 50%;

  .mask {
    height: 45px;
    position: absolute;
    bottom: 0;
    top: unset;
    background-color: #1d1810bc;
    padding: 8px;
    display: flex;
    flex-direction: column;
    gap: 4px;
  }

  .title {
    font-size: 14px;
    line-height: 14px;
    min-height: 24px;
  }

  .description {
    font-size: 12px;
  }
`;

export interface params {
  title: string
  description: string
  image: string
  onclick?: ()=> void
};

const App = (params: params): JSX.Element  => {
  return(
    <Container image={params.image} className='container' onClick={params.onclick}>
      <div className='mask'>
        <h2 className='title'>{params.title}</h2>
        <p className='description' >{params.description}</p>
      </div>
    </Container>
  )
}

App.defaultProps = {
  onclick: () => null,
};

export default App;
