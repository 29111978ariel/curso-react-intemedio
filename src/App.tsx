import { useState } from 'react';
import styled from 'styled-components';
import Card from './component';
import Detalle from './detalle';

const Container = styled.div`
  height: 100vh;  
  
  --card-background-color: #2e2f30;
  --card-border: 1px solid #343536;
  --card-selected: #0098d924;
  --font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
  *{
    font-family: var(--font-family);
    box-sizing: border-box;
  }
`;
const App = (): JSX.Element => {

  const [ visible, setVisible ] = useState<boolean>(true);
  
  return (
    <Container>
      <Card image={'https://images.unsplash.com/photo-1558584724-0e4d32ca55a4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=765&q=80'} onClick={() => setVisible(false)} />
      <Detalle title='Costa Coffee' description='Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quidem unde dolores  repudiandae quam minus ab consequuntur vitae, reiciendis' visible={visible} price={45000} image={'https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80'} onClick={() => setVisible(true)}/>
    </Container>
  )
};

export default App;
